package sofi;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import java.awt.Toolkit;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.LayoutStyle.ComponentPlacement;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.awt.event.ActionEvent;
import javax.swing.ImageIcon;
import java.awt.Color;
import javax.swing.JPasswordField;

public class customer extends JFrame {

	private JPanel contentPane;
	private JTextField textField;
	private JPasswordField passwordField;

	/**
	 * Launch the application.
	 */
	

	/**
	 * Create the frame.
	 */
	public customer() {
		setIconImage(Toolkit.getDefaultToolkit().getImage("F:\\\u067E\\Screenshot_2017-04-15-22-42-44 (2).png"));
		setTitle("\u0645\u0634\u062A\u0631\u06CC");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 769, 475);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(255, 255, 255));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		
		JLabel label = new JLabel("\u0648\u0631\u0648\u062F \u06A9\u0627\u0631\u0628\u0631");
		label.setFont(new Font("Tahoma", Font.BOLD | Font.ITALIC, 20));
		
		JLabel label_1 = new JLabel("\u0646\u0627\u0645 \u06A9\u0627\u0631\u0628\u0631\u06CC:");
		label_1.setFont(new Font("Tahoma", Font.BOLD | Font.ITALIC, 14));
		
		JLabel label_2 = new JLabel("\u06A9\u0644\u0645\u0647 \u0639\u0628\u0648\u0631:");
		label_2.setFont(new Font("Tahoma", Font.BOLD | Font.ITALIC, 14));
		
		textField = new JTextField();
		textField.setColumns(10);
		
		JButton btnNewButton = new JButton("New button");
		btnNewButton.setIcon(new ImageIcon("F:\\\u067E\\images.jpg"));
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			}
		});
		
		JButton button = new JButton("\u0628\u0627\u0632\u06AF\u0634\u062A");
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				cust c = new cust();
				c.pack();
				c.setVisible(true);
				setVisible(false);
				
			}
		});
		button.setFont(new Font("Tahoma", Font.BOLD | Font.ITALIC, 14));
		
		JButton button_1 = new JButton("\u0648\u0631\u0648\u062F");
		button_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				
				String UserString = textField.getText();
				int UserInt = Integer.parseInt(UserString);
				String PassString = String.valueOf(passwordField.getPassword());
				int PassInt = Integer.parseInt(PassString);
				
				//connect to DB
					
				if(main.connectdb()) {
				try {
					   
						Statement statement = main.c.createStatement();
						String 	Query = "SELECT UserName FROM Customer";
						ResultSet result = statement.executeQuery(Query);
						int Test=0;
						while(result.next()) {
							int s = result.getInt("UserName");	
							if(s==UserInt) {
								Test =1;
							}
						}
						int Test1=0;
						if(Test == 1) {
						String Query1 = "SELECT PassWord FROM Customer";
						ResultSet result1 = statement.executeQuery(Query1);
						while(result1.next()) {
							String s1 = result1.getString("PassWord");
							int s11 = Integer.parseInt(s1);
							if(s11==PassInt) {
								Test1 =1;
							}
						}
						}
						if((Test1 == 1) && (Test == 1)) {
							cust2 cu = new cust2(UserInt);
							cu.pack();
							cu.setVisible(true);
							setVisible(false);
						}
						else {
							JOptionPane.showMessageDialog(null,"UserName Or Password Is Incorect!","ERROR",JOptionPane.ERROR_MESSAGE);
						}
						result.close();
						statement.close();
						
				}
				catch(SQLException ex) {
					System.out.println(ex.getMessage());
				}
				}
			}
		});
		button_1.setFont(new Font("Tahoma", Font.BOLD | Font.ITALIC, 14));
		
		JButton btnNewButton_1 = new JButton("\u0641\u0631\u0627\u0645\u0648\u0634 \u06A9\u0631\u062F\u0646 \u0634\u0645\u0627\u0631\u0647 \u0627\u0634\u062A\u0631\u0627\u06A9");
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				forgetid f = new forgetid();
				f.pack();
				f.setVisible(true);
				setVisible(false);
				
				
				
			}
		});
		btnNewButton_1.setFont(new Font("Tahoma", Font.BOLD | Font.ITALIC, 12));
		
		passwordField = new JPasswordField();
		GroupLayout gl_contentPane = new GroupLayout(contentPane);
		gl_contentPane.setHorizontalGroup(
			gl_contentPane.createParallelGroup(Alignment.TRAILING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addContainerGap()
					.addComponent(btnNewButton, GroupLayout.PREFERRED_SIZE, 249, GroupLayout.PREFERRED_SIZE)
					.addGap(77)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_contentPane.createSequentialGroup()
							.addPreferredGap(ComponentPlacement.RELATED, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
							.addGroup(gl_contentPane.createParallelGroup(Alignment.TRAILING, false)
								.addComponent(button, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
								.addComponent(btnNewButton_1, GroupLayout.DEFAULT_SIZE, 214, Short.MAX_VALUE)
								.addComponent(button_1, Alignment.LEADING, GroupLayout.DEFAULT_SIZE, 191, Short.MAX_VALUE)))
						.addGroup(gl_contentPane.createSequentialGroup()
							.addGap(115)
							.addGroup(gl_contentPane.createParallelGroup(Alignment.TRAILING)
								.addComponent(label, GroupLayout.PREFERRED_SIZE, 118, GroupLayout.PREFERRED_SIZE)
								.addGroup(gl_contentPane.createSequentialGroup()
									.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING, false)
										.addComponent(passwordField)
										.addComponent(textField))
									.addGap(18)
									.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
										.addComponent(label_2, GroupLayout.PREFERRED_SIZE, 87, GroupLayout.PREFERRED_SIZE)
										.addComponent(label_1, GroupLayout.PREFERRED_SIZE, 101, GroupLayout.PREFERRED_SIZE))))))
					.addGap(101))
		);
		gl_contentPane.setVerticalGroup(
			gl_contentPane.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addContainerGap()
					.addGroup(gl_contentPane.createParallelGroup(Alignment.TRAILING)
						.addComponent(btnNewButton, GroupLayout.PREFERRED_SIZE, 169, GroupLayout.PREFERRED_SIZE)
						.addGroup(gl_contentPane.createSequentialGroup()
							.addComponent(label)
							.addGap(59)
							.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
								.addComponent(textField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
								.addComponent(label_1))
							.addGap(40)
							.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
								.addComponent(label_2)
								.addComponent(passwordField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
							.addGap(42)
							.addComponent(button_1)
							.addGap(35)
							.addComponent(button)
							.addGap(38)
							.addComponent(btnNewButton_1, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)))
					.addContainerGap(56, Short.MAX_VALUE))
		);
		contentPane.setLayout(gl_contentPane);
	}
}
