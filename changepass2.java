package sofi;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import javax.swing.JTextField;
import java.awt.Toolkit;
import javax.swing.JButton;
import javax.swing.LayoutStyle.ComponentPlacement;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.awt.event.ActionEvent;
import javax.swing.ImageIcon;
import java.awt.Color;
import javax.swing.JPasswordField;

public class changepass2 extends JFrame {

	private JPanel contentPane;
	private JTextField textField;
	private JPasswordField passwordField;
	private JPasswordField passwordField_1;

	/**
	 * Launch the application.
	 */
	

	/**
	 * Create the frame.
	 */
	public changepass2() {
		setTitle("\u062A\u063A\u06CC\u06CC\u0631 \u067E\u0633\u0648\u0631\u062F");
		setIconImage(Toolkit.getDefaultToolkit().getImage("F:\\\u067E\\Screenshot_2017-04-15-22-42-44 (2).png"));
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 677, 489);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(255, 255, 255));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		
		JLabel label = new JLabel("\u067E\u0633\u0648\u0631\u062F \u0641\u0639\u0644\u06CC:");
		label.setFont(new Font("Tahoma", Font.BOLD | Font.ITALIC, 12));
		
		JLabel label_1 = new JLabel("\u067E\u0633\u0648\u0631\u062F \u062C\u062F\u06CC\u062F:");
		label_1.setFont(new Font("Tahoma", Font.BOLD | Font.ITALIC, 12));
		
		JLabel label_2 = new JLabel("\u062A\u0627\u06CC\u06CC\u062F \u067E\u0633\u0648\u0631\u062F \u062C\u062F\u06CC\u062F:");
		label_2.setFont(new Font("Tahoma", Font.BOLD | Font.ITALIC, 12));
		
		textField = new JTextField();
		textField.setColumns(10);
		
		JButton button = new JButton("\u062A\u0627\u06CC\u06CC\u062F");
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				
				String Oldpass = textField.getText();
				String Newpass1 = String.valueOf(passwordField.getPassword());
				int NewPass11 = Integer.parseInt(Newpass1);
			    String Newpass2 = String.valueOf(passwordField_1.getPassword());
			    int NewPass22 = Integer.parseInt(Newpass2);
			    int Test =0;
			    if(NewPass11 == NewPass22) {
			    	Test=1;
			    }
			    else {
			    	JOptionPane.showMessageDialog(null,"Please Enter New Password Again!","Waining",JOptionPane.WARNING_MESSAGE);
			    }
			    
			    if(Test == 1) {
			    	
			    	//connect to DB
			    	if(main.connectdb()) {
			    	try {
						  
							String 	Query = "UPDATE Customer SET PassWord=? WHERE PassWord=? ";
							PreparedStatement p = main.c.prepareStatement(Query);
							p.setString(1,Newpass1);
							p.setString(2,Oldpass);
							p.executeQuery();
						    main.c.close();
					}
					catch(SQLException ex) {
						System.out.println(ex.getMessage());}
			    }
			    }
			    if(Test == 1) {
			    	JOptionPane.showMessageDialog(null,"Your Password Has Succesfuly Changed","Message",JOptionPane.INFORMATION_MESSAGE);
					
			    }
		
				
				
				
			}
		});
		button.setFont(new Font("Tahoma", Font.BOLD | Font.ITALIC, 15));
		
		JButton btnNewButton = new JButton("New button");
		btnNewButton.setIcon(new ImageIcon("F:\\\u067E\\images-7.jpg"));
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			}
		});
		
		JButton btnNewButton_1 = new JButton("\u0628\u0627\u0632\u06AF\u0634\u062A");
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				cust2 c = new cust2();
				c.pack();
				c.setVisible(true);
				setVisible(false);
			}
		});
		btnNewButton_1.setFont(new Font("Tahoma", Font.BOLD | Font.ITALIC, 14));
		
		passwordField = new JPasswordField();
		
		passwordField_1 = new JPasswordField();
		GroupLayout gl_contentPane = new GroupLayout(contentPane);
		gl_contentPane.setHorizontalGroup(
			gl_contentPane.createParallelGroup(Alignment.TRAILING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addGap(66)
					.addComponent(btnNewButton, GroupLayout.PREFERRED_SIZE, 118, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.RELATED, 147, Short.MAX_VALUE)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING, false)
						.addComponent(passwordField_1)
						.addComponent(passwordField, Alignment.TRAILING)
						.addComponent(textField, Alignment.TRAILING))
					.addGap(80)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
						.addComponent(label_2)
						.addComponent(label)
						.addComponent(label_1))
					.addGap(53))
				.addGroup(gl_contentPane.createSequentialGroup()
					.addGap(361)
					.addComponent(button, GroupLayout.PREFERRED_SIZE, 110, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.RELATED, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
					.addComponent(btnNewButton_1, GroupLayout.PREFERRED_SIZE, 119, GroupLayout.PREFERRED_SIZE)
					.addGap(55))
		);
		gl_contentPane.setVerticalGroup(
			gl_contentPane.createParallelGroup(Alignment.TRAILING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
						.addGroup(gl_contentPane.createSequentialGroup()
							.addGap(78)
							.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
								.addComponent(label)
								.addComponent(textField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
							.addGap(20)
							.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
								.addComponent(label_1)
								.addComponent(passwordField, GroupLayout.PREFERRED_SIZE, 22, GroupLayout.PREFERRED_SIZE))
							.addGap(23)
							.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
								.addComponent(label_2)
								.addComponent(passwordField_1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
						.addGroup(gl_contentPane.createSequentialGroup()
							.addGap(75)
							.addComponent(btnNewButton, GroupLayout.PREFERRED_SIZE, 120, GroupLayout.PREFERRED_SIZE)))
					.addGap(55)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
						.addComponent(btnNewButton_1, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
						.addComponent(button, GroupLayout.PREFERRED_SIZE, 22, GroupLayout.PREFERRED_SIZE))
					.addGap(151))
		);
		contentPane.setLayout(gl_contentPane);
	}
}
