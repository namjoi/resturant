package sofi;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import java.awt.Toolkit;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.JTextField;
import java.awt.Color;
import javax.swing.JButton;
import javax.swing.ImageIcon;
import java.awt.event.ActionListener;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.awt.event.ActionEvent;
import javax.swing.JPasswordField;

public class admin extends JFrame {

	private JPanel contentPane;
	private JTextField textField;
	private JPasswordField passwordField;

	/**
	 * Launch the application.
	 */
	
	
	public static ResultSet selectuser() {
		
		if(main.connectdb()) {
			try {
				Statement statement = main.c.createStatement();
				String 	Query = "SELECT UserName FROM Admin";
				ResultSet result = statement.executeQuery(Query);
				return result;
			}
			catch(SQLException ex) {
				System.out.println(ex.getMessage());
			}
		}
		return null;
	}
	
	

	/**
	 * Create the frame.
	 */
	public admin() {
		setTitle("\u0648\u0631\u0648\u062F \u0627\u062F\u0645\u06CC\u0646");
		setIconImage(Toolkit.getDefaultToolkit().getImage("F:\\\u067E\\Screenshot_2017-04-15-22-42-44 (2).png"));
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 652, 487);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(255, 255, 255));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		
		JLabel lblNewLabel = new JLabel("\u0648\u0631\u0648\u062F \u0627\u062F\u0645\u06CC\u0646");
		lblNewLabel.setFont(new Font("Tahoma", Font.BOLD | Font.ITALIC, 20));
		
		JLabel lblNewLabel_1 = new JLabel("\u0646\u0627\u0645 \u06A9\u0627\u0631\u0628\u0631\u06CC:");
		lblNewLabel_1.setFont(new Font("Tahoma", Font.PLAIN, 14));
		
		JLabel label = new JLabel("\u06A9\u0644\u0645\u0647 \u0639\u0628\u0648\u0631:");
		label.setFont(new Font("Tahoma", Font.PLAIN, 14));
		
		textField = new JTextField();
		textField.setColumns(10);
		
		JButton btnNewButton = new JButton("New button");
		btnNewButton.setIcon(new ImageIcon("F:\\\u067E\\images-8.jpg"));
		
		JButton button = new JButton("\u0648\u0631\u0648\u062F");
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				String UserString = textField.getText();
				int UserInt = Integer.parseInt(UserString);
				String PassString = String.valueOf(passwordField.getPassword());
				int PassInt = Integer.parseInt(PassString);
				
				//connect to DB
				if(main.connectdb()) {
				try {
					  
						ResultSet result = selectuser();
						int Test=0;
						while(result.next()) {
							int s = result.getInt("UserName");	
							if(s==UserInt) {
								Test =1;
							}
						}
						int Test1=0;
						if(Test == 1) {
						String Query1 = "SELECT PassWord FROM Admin";
						Statement statement = main.c.createStatement();
						ResultSet result1 = statement.executeQuery(Query1);
						while(result1.next()) {
							String s1 = result1.getString("PassWord");
							int s11 = Integer.parseInt(s1);
							if(s11==PassInt) {
								Test1 =1;
							}
						}
						}
						if((Test1 == 1) && (Test == 1)) {
							admin2 a = new admin2();
							a.pack();
							a.setVisible(true);
							setVisible(false);
						}
						else {
							JOptionPane.showMessageDialog(null,"UserName Or Password Is Incorect!","ERROR",JOptionPane.ERROR_MESSAGE);
						}
						result.close();
						main.c.close();
				}
				catch(SQLException ex) {
					System.out.println(ex.getMessage());
				}
				}
			}
		});
		button.setFont(new Font("Tahoma", Font.ITALIC, 14));
		
		JButton button_1 = new JButton("\u0628\u0627\u0632\u06AF\u0634\u062A");
		button_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				main m = new main();
				m.pack();
				m.setVisible(true);
				setVisible(false);
				
			}
		});
		button_1.setFont(new Font("Tahoma", Font.ITALIC, 14));
		
		passwordField = new JPasswordField();
		GroupLayout gl_contentPane = new GroupLayout(contentPane);
		gl_contentPane.setHorizontalGroup(
			gl_contentPane.createParallelGroup(Alignment.TRAILING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addGap(67)
					.addComponent(btnNewButton, GroupLayout.PREFERRED_SIZE, 188, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.RELATED, 54, Short.MAX_VALUE)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.TRAILING, false)
						.addComponent(passwordField)
						.addComponent(textField)
						.addComponent(button_1, Alignment.LEADING))
					.addGap(31)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
						.addComponent(button, GroupLayout.PREFERRED_SIZE, 77, GroupLayout.PREFERRED_SIZE)
						.addComponent(lblNewLabel_1, GroupLayout.DEFAULT_SIZE, 190, Short.MAX_VALUE)
						.addComponent(label))
					.addContainerGap())
				.addGroup(gl_contentPane.createSequentialGroup()
					.addContainerGap(394, Short.MAX_VALUE)
					.addComponent(lblNewLabel)
					.addGap(130))
		);
		gl_contentPane.setVerticalGroup(
			gl_contentPane.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addGap(32)
					.addComponent(lblNewLabel)
					.addGap(75)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
						.addGroup(gl_contentPane.createSequentialGroup()
							.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
								.addComponent(lblNewLabel_1)
								.addComponent(textField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
							.addGap(36)
							.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
								.addComponent(label)
								.addComponent(passwordField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
							.addPreferredGap(ComponentPlacement.RELATED, 47, Short.MAX_VALUE)
							.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
								.addComponent(button_1, GroupLayout.PREFERRED_SIZE, 28, GroupLayout.PREFERRED_SIZE)
								.addComponent(button, GroupLayout.PREFERRED_SIZE, 27, GroupLayout.PREFERRED_SIZE))
							.addGap(17))
						.addComponent(btnNewButton, GroupLayout.PREFERRED_SIZE, 168, GroupLayout.PREFERRED_SIZE))
					.addGap(138))
		);
		contentPane.setLayout(gl_contentPane);
	}
}
